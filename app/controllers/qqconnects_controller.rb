class QqconnectsController < ApplicationController
  unloadable
  before_action :find_qqconnect, :only => :destroy
  before_action :logged_in_user, :only => [:create, :update, :destroy]
  include QqconnectsHelper
  
  def create
    type = params["from"]
    case type
    when "mm"
      if mm_logged_in?
        user = User.current
        mm_nickname = session[:mm_nickname]
        mm_openid = session[:mm_openid]
        @qqconnect = user.qqconnect
        if @qqconnect.nil?
          if qqconnect = user.create_qqconnect!(mm_name: mm_nickname, mm_openid: mm_openid)
            @nickname = qqconnect.name
            flash[:notice] = "#{user.name}#{l(:notice_mm_bind)}"
          else
            flash.now[:error] = l(:notice_mm_bind_fail)
          end
        else
          @qqconnect.mm_openid = mm_openid
          @qqconnect.mm_name = mm_nickname
          if (@qqconnect.save)
            flash[:notice] = l(:notice_mm_bind)
          else
            flash.now[:error] = l(:notice_mm_bind_fail)
          end
        end
        redirect_to my_account_path
      end
    when "qq"
      if qq_logged_in?
        user = User.current
        qq_nickname = session[:qq_nickname]
        qq_openid = session[:qq_openid]
        @qqconnect = user.qqconnect
        if @qqconnect.nil?
          if qqconnect = user.create_qqconnect!(name: qq_nickname, openid: qq_openid)
            @nickname = qqconnect.name
            flash[:notice] = "#{user.name}#{l(:notice_qq_bind)}"
          else
            flash.now[:error] = l(:notice_qq_bind_fail)
          end
        else
          @qqconnect.openid = qq_openid
          @qqconnect.name = qq_nickname
          if (@qqconnect.save)
            flash[:notice] = l(:notice_qq_bind)
          else
            flash.now[:error] = l(:notice_qq_bind_fail)
          end
        end
        redirect_to my_account_path
      else
        redirect_uri = Setting["plugin_qqs"][:qq_redirect_uri2]
        auth_prefix = Setting["plugin_qqs"][:qq_auth_prefix]
        appid = Setting["plugin_qqs"][:qq_app_id]
        redirect_to("#{auth_prefix}#{appid}&redirect_uri=#{redirect_uri}")
      end
    end
  end

  def update
    type = params["from"]
    case type
    when "mm"
      if mm_logged_in?
        user = User.current
        mm_nickname = session[:mm_nickname]
        mm_openid = session[:mm_openid]
        @qqconnect = user.qqconnect
        if @qqconnect.nil?
          if qqconnect = user.create_qqconnect!(mm_name: mm_nickname, mm_openid: mm_openid)
            @nickname = qqconnect.name
            flash[:notice] = "#{user.name}#{l(:notice_mm_bind)}"
          else
            flash.now[:error] = l(:notice_mm_bind_fail)
          end
        else
          @qqconnect.mm_openid = mm_openid
          @qqconnect.mm_name = mm_nickname
          if (@qqconnect.save)
            flash[:notice] = l(:notice_mm_bind_update)
          else
            flash.now[:error] = l(:notice_mm_bind_fail)
          end
        end
        redirect_to my_account_path
      end
    when "qq"
      if qq_logged_in?
        user = User.current
        qq_nickname = session[:qq_nickname]
        qq_openid = session[:qq_openid]
        @qqconnect = user.qqconnect
        if @qqconnect.nil?
          if qqconnect = user.create_qqconnect!(name: qq_nickname, openid: qq_openid)
            @nickname = qqconnect.name
            flash[:notice] = "#{user.name}#{l(:notice_qq_bind)}"
          else
            flash.now[:error] = l(:notice_qq_bind_fail)
          end
        else
          @qqconnect.openid = qq_openid
          @qqconnect.name = qq_nickname
          if (@qqconnect.save)
            flash[:notice] = l(:notice_qq_bind_update)
          else
            flash.now[:error] = l(:notice_qq_bind_fail)
          end
        end
        redirect_to my_account_path
      else
        redirect_uri = Setting["plugin_qqs"][:qq_redirect_uri2]
        auth_prefix = Setting["plugin_qqs"][:qq_auth_prefix]
        appid = Setting["plugin_qqs"][:qq_app_id]
        redirect_to("#{auth_prefix}#{appid}&redirect_uri=#{redirect_uri}")
      end
    end
  end

  def destroy
    # destroy the qq or mm binding
    type = params["to"]
    case type
    when "qq"
      if @qqconnect.mm_openid.nil?
        if (@qqconnect.destroy)
          redirect_to my_account_path
          flash[:notice] = "#{User.current.name}#{l(:notice_qq_bind_drop)}"
        else
          redirect_to my_account_path
          flash[:error] = l(:notice_qq_bind_drop_fail)
        end
      else  # no mm's info exists
        if (@qqconnect.update_attributes(openid: nil, name: nil))
          redirect_to my_account_path
          flash[:notice] = "#{User.current.name}#{l(:notice_qq_bind_drop)}"
        else
          redirect_to my_account_path
          flash[:error] = l(:notice_qq_bind_drop_fail)
        end
      end
    when "mm"
      if @qqconnect.openid.nil?
        if (@qqconnect.destroy)
          redirect_to my_account_path
          flash[:notice] = "#{User.current.name}#{l(:notice_mm_bind_drop)}"
        else
          redirect_to my_account_path
          flash[:error] = l(:notice_mm_bind_drop_fail)
        end
      else  # no mm's info exists
        if (@qqconnect.update_attributes(mm_openid: nil, mm_name: nil))
          redirect_to my_account_path
          flash[:notice] = "#{User.current.name}#{l(:notice_mm_bind_drop)}"
        else
          redirect_to my_account_path
          flash[:error] = l(:notice_mm_bind_drop_fail)
        end
      end
    end
  end


  private
    def find_qqconnect
      @qqconnect = Qqconnect.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      render_404
    end
    
    def logged_in_user
      if (User.current.anonymous? || User.current.nil?)
       flash[:danger] = l(:notice_need_login)
       redirect_to url_for(:controller => 'account', :action => 'login')
      end
    end
end
