module MmloginsHelper
  # login from mm connect
  def mm_login(code, appid, appsecret)
    # get the mm connect info.
    mmuser = Mm.new(code, appid, appsecret)
    if !mmuser.nil?
      mmuser_info = mmuser.get_mm_user_info
      mm_nickname = mmuser_info['nickname']
      mm_avater_url = mmuser_info['headimgurl']
      mm_openid = mmuser.openid
      # store the mm user's info into session.
      session[:mm_nickname] = mm_nickname
      session[:qq_avater_url] = mm_avater_url
      session[:mm_openid] = mm_openid
    end

    mmuser
  end
  
  
  # if mm has been binded to user. autologin.
  def mm_login_with_user(qqconn)
    user = qqconn.user
    if !user.nil?
      # save the qq session
      mm_nickname = session[:mm_nickname]
      mm_avater_url = session[:qq_avater_url]
      mm_openid = session[:mm_openid]
      logger.info "Successful authentication for '#{user.login}' from #{request.remote_ip} at #{Time.now.utc}"
      # Valid user
      self.logged_user = user
      # generate a key and set cookie if autologin
      if params[:autologin] && Setting.autologin?
        set_autologin_cookie(user)
      end
      call_hook(:controller_account_success_authentication_after, {:user => user })
      flash[:notice] = "#{user.name} #{l(:notice_mm_login)}"
      # restore the qq session
      session[:mm_nickname] = mm_nickname
      session[:qq_avater_url] = mm_avater_url
      session[:mm_openid] = mm_openid
    end
  end
end
