class Qqconnect < ActiveRecord::Base
  unloadable
  belongs_to :user
  validates :openid, uniqueness: true, allow_nil: true
  validates :mm_openid, uniqueness: true, allow_nil: true
  validates :user_id, presence: true
end
