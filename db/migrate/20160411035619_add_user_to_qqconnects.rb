class AddUserToQqconnects < ActiveRecord::Migration
  def change
    add_reference :qqconnects, :user, index: true, foreign_key: true
  end
end
