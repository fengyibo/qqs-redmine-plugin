class AddIndexToQqconnectsMmOpenid < ActiveRecord::Migration
  def change
    add_index :qqconnects, :mm_openid, unique: true
  end
end
