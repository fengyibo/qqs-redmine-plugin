require "application_controller_patch" 
require "welcome_controller_patch" 
require "account_controller_patch" 
require "my_controller_patch" 
require "user_patch"

Redmine::Plugin.register :qqs do
  name 'Qqs plugin'
  author 'Wu Gaoming'
  description 'This is a qq connect plugin for Redmine'
  version '0.5.0'
  url 'https://bitbucket.org/39648421/qqs-redmine-plugin'
  author_url 'https://bitbucket.org/39648421/'
  settings :default => {
    :qq_validate_code                => "352423553516513164042163153523617",
    :qq_app_id                       => "101279453",
    :qq_app_key                      => "fa4eb13f97d376eddef4494153588148",
    :qq_redirect_uri                 => "http://tsman-a39648421.c9users.io/login",
    :qq_redirect_uri2                => "http://tsman-a39648421.c9users.io/my/account",
    :qq_redirect_uri3                => "http://tsman-a39648421.c9users.io",
    :qq_auth_prefix                  => "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id=",
    :mm_app_id                       => "wx9e7ba0a25d428bb5",
    :mm_redirect_uri                 => "tsman-a39648421.c9users.io",
    :mm_app_secret                   => "42e86762928359fe982907275c04f5c0",
    :mm_auth_prefix                  => "https://open.weixin.qq.com/connect/qrconnect?response_type=code&scope=snsapi_login&appid=",
    :mm_auth_tail                    => "&state=000000#wechat_redirect",
    :mm_token_prefix                 => "https://api.weixin.qq.com/sns/oauth2/access_token?appid="
  }, :partial => 'settings/qq_settings'
end
