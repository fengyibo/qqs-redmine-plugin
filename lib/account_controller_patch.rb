require_dependency 'account_controller'
require File.expand_path('../../config/qq_env', __FILE__)

module AccountControllerPatch
  def self.included(base) # :nodoc:
    base.send(:include, InstanceMethods)
    base.class_eval do
      unloadable # Send unloadable so it will not be unloaded in development
      # defind a globle var for backurl
      helper :qqlogins, :mmlogins, :qqconnects
      include QqconnectsHelper
      alias_method_chain :login, :login_qqauth
      alias_method_chain :register_automatically, :register_automatically_qq
      alias_method_chain :register_by_email_activation, :register_by_email_activation_qq
      alias_method_chain :register_manually_by_administrator, :register_manually_by_administrator_qq
    end
  end

  module InstanceMethods
    def login_with_login_qqauth
      # get return code
      code = params[:code]
      
      if code.nil? 
        $backurl = back_url
        if qq_logged_in?
          qq_nickname = session[:qq_nickname] 
          qq_avater_url = session[:qq_avater_url]
          qq_openid = session[:qq_openid]
          
          login_without_login_qqauth
          
          session[:qq_nickname] = qq_nickname
          session[:qq_avater_url] = qq_avater_url
          session[:qq_openid] = qq_openid
        else
          login_without_login_qqauth
        end
          
      else
        # redirect_uri = QqEnv::REDIRECT_URI
        # appid = QqEnv::APPID
        # appkey = QqEnv::APPKEY
        redirect_uri = Setting["plugin_qqs"][:qq_redirect_uri]
        appid = Setting["plugin_qqs"][:qq_app_id]
        appkey = Setting["plugin_qqs"][:qq_app_key]
        
        @qquser = qq_login(code, redirect_uri, appid, appkey)
        
        if @qquser.nil?
          return
        end
        openid = session[:qq_openid]
        qqconn = Qqconnect.find_by(openid: openid)
        if qqconn.nil?
          redirect_to home_url
          if !@nickname.nil?
            flash[:notice] = "#{l(:notice_qq_welcome)}#{@nickname}#{l(:notice_qq_unbind_tip)}"
          end
        else
          # pass
          user = qqconn.user
          if user.nil?
            redirect_to home_url
          else
            qq_login_with_user(qqconn)
            redirect_back_or_default $backurl
            # flash[:notice] = "#{user.name} #{l(:notice_qq_login)}"
          end
        end
      end
    end
    
    def register_automatically_with_register_automatically_qq(user, &block)
      if qq_logged_in?
        # Automatic activation
        user.activate
        user.last_login_on = Time.now
        if user.save
          qq_nickname = session[:qq_nickname]
          qq_openid = session[:qq_openid]
          if qqconn = user.create_qqconnect!(name: qq_nickname, openid: qq_openid)
            @nickname = qqconn.name
          end
          self.logged_user = user
          flash[:notice] = l(:notice_account_activated)
          redirect_to my_account_path
        else
          yield if block_given?
        end
        return
      end
      if mm_logged_in?
        # Automatic activation
        user.activate
        user.last_login_on = Time.now
        if user.save
          mm_nickname = session[:mm_nickname]
          mm_openid = session[:mm_openid]
          if qqconn = user.create_qqconnect!(name: mm_nickname, openid: mm_openid)
            @nickname = qqconn.name
          end
          self.logged_user = user
          flash[:notice] = l(:notice_account_activated)
          redirect_to my_account_path
        else
          yield if block_given?
        end
        return
      end
      register_automatically_without_register_automatically_qq(user, &block)
    end
    
    def register_by_email_activation_with_register_by_email_activation_qq(user, &block)
      if qq_logged_in?
        token = Token.new(:user => user, :action => "register")
        if user.save and token.save
          qq_nickname = session[:qq_nickname]
          qq_openid = session[:qq_openid]
          if qqconn = user.create_qqconnect!(name: qq_nickname, openid: qq_openid)
            @nickname = qqconn.name
          end
          Mailer.register(token).deliver
          flash[:notice] = l(:notice_account_register_done, :email => ERB::Util.h(user.mail))
          redirect_to signin_path
        else
          yield if block_given?
        end
        return
      end
      
      if mm_logged_in?
        token = Token.new(:user => user, :action => "register")
        if user.save and token.save
          mm_nickname = session[:mm_nickname]
          mm_openid = session[:mm_openid]
          if qqconn = user.create_qqconnect!(name: mm_nickname, openid: mm_openid)
            @nickname = qqconn.name
          end
          Mailer.register(token).deliver
          flash[:notice] = l(:notice_account_register_done, :email => ERB::Util.h(user.mail))
          redirect_to signin_path
        else
          yield if block_given?
        end
        return
      end
      register_by_email_activation_without_register_by_email_activation_qq(user, &block)
    end

    def register_manually_by_administrator_with_register_manually_by_administrator_qq(user, &block)
      if qq_logged_in?
        if user.save
          # Sends an email to the administrators
          qq_nickname = session[:qq_nickname]
          qq_openid = session[:qq_openid]
          if qqconn = user.create_qqconnect!(name: qq_nickname, openid: qq_openid)
            @nickname = qqconn.name
          end
          Mailer.account_activation_request(user).deliver
          account_pending(user)
        else
          yield if block_given?
        end
      end
      if mm_logged_in?
        if user.save
          # Sends an email to the administrators
          mm_nickname = session[:mm_nickname]
          mm_openid = session[:mm_openid]
          if qqconn = user.create_qqconnect!(name: mm_nickname, openid: mm_openid)
            @nickname = qqconn.name
          end
          Mailer.account_activation_request(user).deliver
          account_pending(user)
        else
          yield if block_given?
        end
      end
      register_manually_by_administrator_without_register_manually_by_administrator_qq(user, &block)
    end
    
  end
end

Rails.configuration.to_prepare do
  AccountController.send(:include, AccountControllerPatch)
end