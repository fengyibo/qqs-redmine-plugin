require 'open-uri'
require 'net/http'
# mm Connect like qq connect
class Mm
  attr_reader :openid
  # code = params[:code]
  # get appid & appkey from http://opensns.qq.com
  def initialize(code, appid, appsecret)
    # CSRF
    state = Digest::MD5.hexdigest(rand.to_s)
    # get token
    url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' + 
    # url = 'https://graph.qq.com/oauth2.0/token?grant_type=authorization_code&' + 
"#{appid}&" + 
"secret=#{appsecret}&" + 
"code=#{code}&grant_type=authorization_code"
    result_json = open(url).read
    # translate into hash
    result = JSON.parse result_json
    @token = result['access_token']
  
    # get openid
    @openid = result['openid']
  end

  def get_mm_user_info
    url = "https://api.weixin.qq.com/sns/userinfo?access_token=#{@token}&openid=#{@openid}"
    result_json = open(url).read
    result = JSON.parse result_json
    
  end
end
