require_dependency 'my_controller'
require File.expand_path('../../config/qq_env', __FILE__)

module MyControllerPatch
  def self.included(base) # :nodoc:
    base.send(:include, InstanceMethods)
    base.class_eval do
      unloadable # Send unloadable so it will not be unloaded in development
      helper :qqlogins, :qqconnects
      include QqloginsHelper
      alias_method_chain :account, :account_qqauth
    end
  end

  module InstanceMethods
    def account_with_account_qqauth
      # get return code
      code = params[:code]
      @user = User.current
      @qqconnect = @user.qqconnect
      if !@qqconnect.nil?
        @nickname = @qqconnect.name
        @mm_nickname = @qqconnect.mm_name
      end
    
      if code.nil? 
        account_without_account_qqauth
      else
        if request.post?
          @user.safe_attributes = params[:user] if params[:user]
          @user.pref.attributes = params[:pref] if params[:pref]
          if @user.save
            @user.pref.save
            set_language_if_valid @user.language
            flash[:notice] = l(:notice_account_updated)
            redirect_to my_account_path
            return
          end
        else
          # redirect_uri = QqEnv::REDIRECT_URI
          # appid = QqEnv::APPID
          # appkey = QqEnv::APPKEY
          
          redirect_uri = Setting["plugin_qqs"][:qq_redirect_uri2]
          appid = Setting["plugin_qqs"][:qq_app_id]
          appkey = Setting["plugin_qqs"][:qq_app_key]
          
          qquser = qq_login(code,redirect_uri,appid,appkey)
          
          openid = session[:qq_openid]
          @nickname = session[:qq_nickname]
          
          if @qqconnect.nil?
            if @qqconnect = @user.create_qqconnect!(name:  @nickname, openid: openid)
              @nickname = @qqconnect.name
              flash[:notice] = "#{@user.name}#{l(:notice_qq_bind)}"
            else
              flash.now[:error] = l(:notice_qq_bind_fail)
            end
          else
            @qqconnect.openid = openid
            @qqconnect.name = @nickname
            if (@qqconnect.save)
              flash[:notice] = l(:notice_qq_bind)
            else
              flash.now[:error] = l(:notice_qq_bind_fail)
            end
          end
          redirect_to my_account_path
        end
      end
    end
  end
end

Rails.configuration.to_prepare do
  MyController.send(:include, MyControllerPatch)
end