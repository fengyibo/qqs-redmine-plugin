require_dependency 'welcome_controller'
require File.expand_path('../../config/qq_env', __FILE__)

module WelcomeControllerPatch
  def self.included(base) # :nodoc:
    base.send(:include, InstanceMethods)
    base.class_eval do
      unloadable # Send unloadable so it will not be unloaded in development
      # defind a globle var for backurl
      helper :qqlogins, :mmlogins
      include QqloginsHelper, MmloginsHelper
      alias_method_chain :index, :index_qqauth
    end
  end

  module InstanceMethods
    def index_with_index_qqauth
      # get mm state
      state = params[:state]
      # get return code
      code = params[:code]
      index_without_index_qqauth
      
      if state == "000000" && !code.nil? # mm's process
        # get mm token url prefix
        mm_token_prefix = Setting["plugin_qqs"][:mm_token_prefix]
        mm_appid = Setting["plugin_qqs"][:mm_app_id]
        mm_secret = Setting["plugin_qqs"][:mm_app_secret]
        
        @mmuser = mm_login(code,mm_appid,mm_secret)
        
        if (@mmuser.nil?)
          flash.now[:error] = l(:notice_mm_login_fail)
          return
        end

        # if user logged
        if User.current.logged?
          if !User.current.qqconnect.nil? && User.current.qqconnect.mm_openid == session[:mm_openid]
            flash[:notice] = "#{l(:notice_mm_welcome)}#{session[:mm_nickname]}#{l(:notice_mm_binded_tip)}"  
            return
          end
          flash[:notice] = "#{l(:notice_mm_welcome)}#{session[:mm_nickname]}#{l(:notice_mm_unbind_tip)}"
          return
        end
        
        openid = session[:mm_openid]
        qqconn = Qqconnect.find_by(mm_openid: openid)
        if qqconn.nil?
          redirect_to home_url
          flash.now[:error] = l(:notice_mm_login_fail)
        else
          # pass
          mm_login_with_user(qqconn)
        end
        return
      end
      
      if !code.nil? 
        # redirect_uri = QqEnv::REDIRECT_URI3
        # appid = QqEnv::APPID
        # appkey = QqEnv::APPKEY
        
        redirect_uri = Setting["plugin_qqs"][:qq_redirect_uri3]
        appid = Setting["plugin_qqs"][:qq_app_id]
        appkey = Setting["plugin_qqs"][:qq_app_key]
        
        @qquser = qq_login(code,redirect_uri,appid,appkey)
        if (@qquser.nil?)
          flash.now[:error] = l(:notice_qq_login_fail)
          return
        end
        
        # if user logged
        if User.current.logged?
          if !User.current.qqconnect.nil? && User.current.qqconnect.openid == session[:qq_openid]
            flash[:notice] = "#{l(:notice_qq_welcome)}#{session[:qq_nickname]}#{l(:notice_qq_binded_tip)}"  
            return
          end
          flash[:notice] = "#{l(:notice_qq_welcome)}#{session[:qq_nickname]}#{l(:notice_qq_unbind_tip)}"
          return
        end
        
        openid = session[:qq_openid]
        qqconn = Qqconnect.find_by(openid: openid)
        if qqconn.nil?
          redirect_to home_url
          flash.now[:error] = l(:notice_qq_login_fail)
        else
          # pass
          qq_login_with_user(qqconn)
        end
      end
    end
  end
end

Rails.configuration.to_prepare do
  WelcomeController.send(:include, WelcomeControllerPatch)
end