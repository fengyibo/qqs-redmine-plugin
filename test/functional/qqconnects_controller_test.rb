require File.expand_path('../../test_helper', __FILE__)
include ActionDispatch::Routing::UrlFor

class QqconnectsControllerTest < ActionController::TestCase
  fixtures :projects,
           :users,
           :roles,
           :members,
           :member_roles,
           :issues,
           :issue_statuses,
           :versions,
           :trackers,
           :projects_trackers,
           :issue_categories,
           :enabled_modules,
           :enumerations,
           :attachments,
           :workflows,
           :custom_fields,
           :custom_values,
           :custom_fields_projects,
           :custom_fields_trackers,
           :time_entries,
           :journals,
           :journal_details,
           :queries
  ActiveRecord::Fixtures.create_fixtures(File.dirname(__FILE__) + '/../fixtures/', 
                            [:qqconnects])   

  def setup
    @qqconnect = Qqconnect.find(1)
    @user = users(:users_001)
  end
  
  test "other temp test" do
    qq = @user.qqconnect
    assert_not_nil qq
  end
  
  test "should redirect login page when qq destroy without login" do
    @qqconnect.update_attributes!(mm_name: nil, mm_openid: nil)  # let the mm's info to nil
    
    assert_no_difference 'Qqconnect.count' do
      delete :destroy, id: @qqconnect, to: "qq"
    end
    assert_redirected_to '/login'
  end
    
  test "should redirect login page when mm destroy without login" do
    @qqconnect.update_attributes!(name: nil, openid: nil)  # let the qq's info to nil
    assert_no_difference 'Qqconnect.count' do
      delete :destroy, id: @qqconnect, to: "mm"
    end
    assert_redirected_to '/login'
  end
  
  test "should qq destroy when login" do
    session[:user_id] = 1
    @qqconnect = @user.qqconnect
    @qqconnect.update_attributes!(mm_name: nil, mm_openid: nil)  # let the mm's info to nil
    assert_difference 'Qqconnect.count', -1 do
      delete :destroy, id: @qqconnect, to: "qq"
    end
  end
 
  test "should mm destroy when login" do
    session[:user_id] = 1
    @qqconnect = @user.qqconnect
    @qqconnect.update_attributes!(name: nil, openid: nil)  # let the mm's info to nil
    assert_difference 'Qqconnect.count', -1 do
      delete :destroy, id: @qqconnect, to: "mm"
    end
  end
  
  test "should not create new qqconnect without login" do
    qqconnect_new = @qqconnect.dup
    assert_nil session[:user_id]
    session[:qq_nickname] = qqconnect_new.name
    session[:qq_openid] = qqconnect_new.openid + "difference"
    assert_no_difference 'Qqconnect.count' do
      post :create, from: "qq"
    end
    assert_redirected_to '/login'
  end
 
   test "should create new mm qqconnect" do
    @user = users(:users_005)
    session[:user_id] = @user.id
    session[:mm_nickname] = @qqconnect.mm_name
    session[:mm_openid] = @qqconnect.mm_openid + "difference"
    User.current = @user
    assert_nil User.current.qqconnect
    assert_equal 5, User.current.id, "current.id wrong"
    assert_equal 5, session[:user_id], "session user_id wrong"
    assert_difference 'Qqconnect.count', 1 do
      post :create, from: "mm"
    end
    assert_not_nil User.current.qqconnect
    assert_redirected_to '/my_account'
  end
 
  test "should create new qqconnect" do
    @user = users(:users_005)
    session[:user_id] = @user.id
    session[:qq_nickname] = @qqconnect.name
    session[:qq_openid] = @qqconnect.openid + "difference"
    User.current = @user
    assert_equal 5, session[:user_id]
    assert_equal 5, User.current.id
    assert_difference 'Qqconnect.count', 1 do
      post :create, from: "qq"
    end
  end
  

end
