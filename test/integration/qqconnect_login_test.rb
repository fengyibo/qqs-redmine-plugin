require File.expand_path('../../test_helper', __FILE__)
require File.expand_path(File.dirname(__FILE__) + '/../../../../test/test_helper')
include QqconnectsHelper

class QqconnectLoginTest < Redmine::ApiTest::Base
    fixtures :projects,
           :users,
           :roles,
           :members,
           :member_roles,
           :issues,
           :issue_statuses,
           :versions,
           :trackers,
           :projects_trackers,
           :issue_categories,
           :enabled_modules,
           :enumerations,
           :attachments,
           :workflows,
           :custom_fields,
           :custom_values,
           :custom_fields_projects,
           :custom_fields_trackers,
           :time_entries,
           :journals,
           :journal_details,
           :queries
  ActiveRecord::Fixtures.create_fixtures(File.dirname(__FILE__) + '/../fixtures/', 
                            [:qqconnects])   
                            
  def setup
    @qqconnect = Qqconnect.find(1)
    @user = users(:users_001)
    @other_user = users(:users_002)
  end
  
  test "first integration" do
    # log_user('admin', 'admin')
    # assert_not_nil @request.session[:user_id]
    # request.session[:user_id] = nil
    # assert_nil session[:user_id]
    # assert_nil @request.session[:user_id]
  end
  
  test "my account page should have one qq bind button and two unbind buttons  when mm and qq no login" do
    log_user('admin', 'admin')
    
    get my_account_path
    assert_response :success
    
    assert_select "a[href=?]", "/bind_update_account"
    assert_select "a[href=?]", "/qqconnects/1?to=qq"
    assert_select "a[href=?]", "/qqconnects/1?to=mm"
  end
  
  test "my account page should have two bind button and two unbind buttons  when mm login" do
    log_user('admin', 'admin')
    
    get my_account_path
    assert_response :success
    
    assert_select "a[href=?]", "/bind_update_account"
    assert_select "a[href=?]", "/qqconnects/1?to=qq", count:0
    assert_select "a[href=?]", "/bind_update_mm_account"
    assert_select "a[href=?]", "/qqconnects/1?to=mm"
  end
end
