# Load the Redmine helper
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path(File.dirname(__FILE__) + '/../../../test/test_helper')
require "minitest/reporters"
require File.expand_path('../../app/models/qqconnect', __FILE__)
require File.expand_path('../../app/helpers/qqconnects_helper', __FILE__)
Minitest::Reporters.use!
