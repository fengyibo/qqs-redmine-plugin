require File.expand_path('../../test_helper', __FILE__)

class QqconnectTest < ActiveSupport::TestCase
  fixtures :users

  def setup
    @user = users(:users_002)
    @qqconnect = @user.build_qqconnect(name: "Qq nickname", openid: "qq1234567890",
                               mm_name: "Mm nickname", mm_openid: "mm1234567890")
    @qqconnect2 = Qqconnect.new(user_id: "002", name: "Qq nickname2", openid: "qq1234567890-2",
                               mm_name: "Mm nickname2", mm_openid: "mm1234567890-2")
  end
  
  test "user_id should be present" do
    @qqconnect.user_id = "    "
    assert_not @qqconnect.valid?
  end
  
  test "qq nickname should be present" do
    @qqconnect.name = "  "
    assert_not @qqconnect.valid?
  end
  
  test "mm nickname should be present" do
    @qqconnect.mm_name = "  "
    assert_not @qqconnect.valid?
  end
  
  test "qq openid should be unique" do
    duplicate_qqconnect = @qqconnect.dup
    @qqconnect.save
    assert_not duplicate_qqconnect.valid?
  end
  
  test "mm openid should be unique" do
    duplicate_qqconnect = @qqconnect.dup
    @qqconnect.save
    assert_not duplicate_qqconnect.valid?
    duplicate_qqconnect.mm_openid = nil
  end
  
  test "qq openid should be same when nil" do
    @qqconnect.openid = nil
    duplicate_qqconnect = @qqconnect.dup
    duplicate_qqconnect.mm_openid = "mm09876543d2d1"
    @qqconnect.save
    assert duplicate_qqconnect.valid?
  end
  
  test "mm openid should be same when nil" do
    @qqconnect.mm_openid = nil
    duplicate_qqconnect = @qqconnect.dup
    duplicate_qqconnect.openid = "qq09876543d2d1"
    @qqconnect.save
    assert duplicate_qqconnect.valid?
  end
  
  test "qqconnect should be desroyed when deleting user" do
    @user.save
    @qqconnect.save!
    assert_difference 'Qqconnect.count', -1 do
      @user.destroy
    end
  end
end
